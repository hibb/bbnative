/*
 * Copyright (c) 2011-2015 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import bb.cascades 1.0
import bb.data 1.0

Page {
    content: ListView {
        id: myListView
        
        // Associate the list view with the data model that's defined in the
        // attachedObjects list
        dataModel: dataModel
        
        listItemComponents: [
            
            // The first ListItemComponent defines how
            // "header" items should appear. 
            // These items use a Label.
            ListItemComponent {
                type: "item"
                Label {
                    text: ListItemData.title
                    // Apply a text style to create a large,
                    // bold font with a specific color
                    textStyle {
                        base: SystemDefaults.TextStyles.SmallText
                        fontWeight: FontWeight.Bold
                        color: Color.create ("#7a184a")
                    }
                }
            }
        ]
    }
    
    attachedObjects: [
        GroupDataModel { 
            id: dataModel
            
            // Sort the data in the data model by the "pubDate" field, in
            // descending order, without any automatic grouping
            sortingKeys: ["pubDate"]
            sortedAscending: false
            grouping: ItemGrouping.None 
        },
        DataSource {
            id: dataSource
            
            // Load the XML data from a remote data source, specifying that the
            // "item" data items should be loaded
            source: "http://www.yinger.cn/rss.php?rssid=32"
            query: "/rss/channel/item"
            //  type: DataSourceType.Xml
            
            onDataLoaded: {
                // After the data is loaded, clear any existing items in the data
                // model and populate it with the new data
                dataModel.clear();
                dataModel.insertList(data)
            }
        }
    ]
    
    onCreationCompleted: {
        // When the top-level Page is created, direct the data source to start
        // loading data
        dataSource.load();
    }
}
