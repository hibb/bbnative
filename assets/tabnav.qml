import bb.cascades 1.0

NavigationPane {
    id: navigationPane
//加载菜单目录
    Menu.definition: MenuDefinition {
        helpAction: HelpActionItem {
            onTriggered: {
                var page = Qt.createComponent("about.qml").createObject(navigationPane);
                navigationPane.push(page)  //抛出关于页面
            }
        }
}
    backButtonsVisible: false
    
    // Create the initial screen
    Page {
        content: Container {
            Label {
                text: "Initial page"  //初始页面
            }
        }
        
        actions: [
            // Create the "Push" action
            ActionItem {
                title: "孕期"
                ActionBar.placement: ActionBarPlacement.OnBar
                
                // When this action is selected, create an object
                // that's based on the ComponentDefinition below,
                // and then push it on to the stack to display it
                onTriggered: {
                   // var newPage = pageDefinition.createObject();
                    //navigationPane.push(newPage);
                    
                    var page = Qt.createComponent("DetailsPage.qml").createObject(navigationPane);
                    navigationPane.push(page)
                    
                }
            },
            ActionItem {
                title: "提醒"
                ActionBar.placement: ActionBarPlacement.OnBar
                
                // When this action is selected, create an object
                // that's based on the ComponentDefinition below,
                // and then push it on to the stack to display it
                onTriggered: {
                    // var newPage = pageDefinition.createObject();
                    //navigationPane.push(newPage);
                    
                    var page = Qt.createComponent("DetailsPage.qml").createObject(navigationPane);
                    navigationPane.push(page)
                
                }
            },
             ActionItem {
            title: qsTr("黑白模式")
            imageSource: darkmode ? "asset:///icon/ic_disable.png" : "asset:///icon/ic_enable.png"
            onTriggered: {
                if (darkmode) {
                    webv.settings.userStyleSheetLocation = "blank.css"
                } else {
                    webv.settings.userStyleSheetLocation = "dark.css"
                }
                darkmode = ! darkmode
                _app.setValue("darkmode", darkmode);
            }
            ActionBar.placement: ActionBarPlacement.OnBar
        }


        ]
        
        attachedObjects: [
            // Create the ComponentDefinition that represents the
            // custom component in myPage.qml
            ComponentDefinition {
                id: pageDefinition
                source: "myPage.qml"
            }
        ]
        
    } // end of Page
} // end of NavigationPane