import bb.cascades 1.0
Page {//关于页面
    attachedObjects: [
        ApplicationInfo {
            id:yu
        }
    ]
    ScrollView {
        horizontalAlignment: HorizontalAlignment.Fill
        Container {
            Header {
                title: qsTr("About")
                subtitle: qsTr("Ver:") + yu.version
            }
            ImageView {
                imageSource: "asset:///img/1.png"
                horizontalAlignment: HorizontalAlignment.Center
            }
            Label {
                horizontalAlignment: HorizontalAlignment.Center
                text: ai.title
            }
            Header {
                title: qsTr("Author")
            }
            Label {
                text: qsTr("ericcc. You can contact me via <a href=\"mailto:eric@bbdev.cn\">Email</a>")
                multiline: true
                textFormat: TextFormat.Html
            }Header {
                title: qsTr("About")
                subtitle: qsTr("Ver:") + yu.version
            }
        }
    }
}